# STUDYTRACKER

This is a small command line application made with python to record and keep track of my study sessions.

## FEATURES

* Autosave feature (in case unexpected termination)
* Possible to use any title for session
* Possibilty to edit recorded sessions with external editor

TODO:

* Creation of automated statistics and graphs on a monthly/weekly basis for example
* Possibility to edit and correct recorded sessions via UI

## SETUP & USAGE

Download the file and make it executable.

Use with:

>	python3.4 /path/to/file CUSTOMTITLEHERE

Press ENTER to end session and exit.