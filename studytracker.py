#!/usr/bin/python3.4

# TODO: Add more statistics
# TODO: Investigate the possibility of a problem in passing data between threading

import sys
import time
import datetime
import calendar
import collections
import threading
from os.path import expanduser
from datetime import datetime

# This function is used to get the timestampscra of start of a studysession
def studyStart():
    currentEpoch = calendar.timegm(time.gmtime())
    currentDateTime = time.strftime(longFMT)
    return {'epoch':currentEpoch, 'datetime':currentDateTime}

# This function is used to get the timestamp of ending of a studysession
def studyStop():
    endEpoch = calendar.timegm(time.gmtime())
    endDateTime = time.strftime(longFMT)
    return {'epoch':endEpoch, 'datetime':endDateTime}

# This function is used to handle reading the studytimes.txt and writing new data into it
def studyOutput(currentEpoch, endEpoch, currentDateTime, endDateTime):
    global studyArray

    # Calculate the new total study time (in unix epoch format)
    studyArray[1] = int(studyArray[1]) + (endEpoch - studyArray[5])

    # Convert epoch studyArray[1] to human readable time format using divmod
    totalMinutes, totalSeconds = divmod(int(studyArray[1]), 60)
    totalHours, totalMinutes = divmod(totalMinutes, 60)
    totalStudied = "%d:%02d:%02d" % (totalHours, totalMinutes, totalSeconds)
    studyArray[2] = totalStudied

    # Convert to string and then append this studysession in the end of the array
    endTotalHelpSave = endDateTime.split()[1]
    currentTotalHelpSave = currentDateTime.split()[1]
    totalSave = datetime.strptime(endTotalHelpSave, FMT) - datetime.strptime(currentTotalHelpSave, FMT)
    appendThis = "%s - %s | Total: %s | Subject: %s" % (currentDateTime, endDateTime, totalSave, subject)
    studyArray[-1] = appendThis

    # Write the array overwriting the old file
    arrNewStudies = open(homeDir, "w")
    for item in studyArray:
        arrNewStudies.write("%s\n" % item)
    arrNewStudies.close()
    return

# This function is used to provide Autosave option if one forgots to stop tracking
def studyAutosave(currentEpoch, currentDateTime):
    appended = False
    sleeptime = 10
    while True:
        global studyArray

        # Open the file and read into array
        arrStudies = open(homeDir, "r")
        studyArray = [line.rstrip('\n') for line in arrStudies]
        arrStudies.close()

        # Calculate the new total study time (in unix epoch format)
        autosaveEpoch = calendar.timegm(time.gmtime())
        studyArray[5] = autosaveEpoch
        if appended:
            studyArray[1] = int(studyArray[1]) + sleeptime

        # Convert epoch studyArray[1] to human readable time format
        #BADOLD(12honly):totalStudied = time.strftime(FMT, time.gmtime(int(studyArray[1])))
        totalAutosaveMinutes, totalAutosaveSeconds = divmod(int(studyArray[1]), 60)
        totalAutosaveHours, totalAutosaveMinutes = divmod(totalAutosaveMinutes, 60)
        totalStudied = "%d:%02d:%02d" % (totalAutosaveHours, totalAutosaveMinutes, totalAutosaveSeconds)
        studyArray[2] = totalStudied

        # Convert to string and then append this studysession in the end of the array
        endAutosaveTime = time.strftime(longFMT)
        endTotalHelpAutosave = endAutosaveTime.split()[1]
        currentTotalHelpAutosave = currentDateTime.split()[1]
        ## TODO: There is a weird problem that if the duration of a session is over a day, the day will be shown as a -1 day, 14:15:30 for example
        totalAutosave = datetime.strptime(endTotalHelpAutosave, FMT) - datetime.strptime(currentTotalHelpAutosave, FMT)
        appendAutosaveThis = "%s - %s | Total: %s | Subject: %s" % (currentDateTime, endAutosaveTime, totalAutosave, subject)

        if not appended:
            studyArray.append(appendAutosaveThis)
            appended = True
        else:
            studyArray[-1] = appendAutosaveThis

        # Write the array overwriting the old file
        arrNewStudies = open(homeDir, "w")
        for item in studyArray:
            arrNewStudies.write("%s\n" % item)
        arrNewStudies.close()
        time.sleep(sleeptime)
    return


# Declare some variables that are used functions
studyArray = []
subject = sys.argv[1]
FMT = '%H:%M:%S'
longFMT = '%d.%m.%Y %H:%M:%S'
homeDir = "%s%s" % (expanduser("~"), "/sync/oma/pytho/studytracker/studytracker/studytimes.txt")

# Get the starting time
studyStart = studyStart()

# Start new (daemon) thread here and let it loop forever (exits when main thread is shut down)
try:
    # thread.start_new_thread(studyAutosave, (studyStart['epoch'], studyStart['datetime']))
    threading.Thread(target=studyAutosave,
        args=(studyStart['epoch'], studyStart['datetime']),
        kwargs={},
        daemon=True,
    ).start()
except:
    print("Error: Unable to start thread")

# Hold the main thread with input and continue when ENTER is pressed
exitflag = input("Press [ENTER] to end studying")

# Get the stopping time
studyStop = studyStop()

# Write the starting and stopping to file
studyOutput(studyStart['epoch'], studyStop['epoch'], studyStart['datetime'], studyStop['datetime'])
quit()
