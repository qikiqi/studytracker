import re
import sys
import time, datetime
import numpy as np


def epoch_to_HHMMSS(total_seconds_datetime):
    total_conv_minutes, total_conv_seconds = divmod(int(total_seconds_datetime), 60)
    total_conv_hours, total_conv_minutes = divmod(total_conv_minutes, 60)
    totalStudied_datetime = "%d:%02d:%02d" % (total_conv_hours, total_conv_minutes, total_conv_seconds)
    return totalStudied_datetime

# This tool can be used to make sure that the studytimes.txt is in correct time
# TODO: Integrate this tool into the studytracker tool itself. ARGPARSE
print("Current location is {}".format(sys.argv[0]))
studytimes_file = "/home/x230/sync/oma/pytho/studytracker/studytracker/studytimes.txt"
with open(studytimes_file, 'r') as f:
    studytimes_arr = f.read().splitlines()


## Create total time studied in seconds by the 'Total: '
studysession_arr = []
for i in range(0,len(studytimes_arr)):
    search_line = studytimes_arr[i]
    result = re.search('Total: (.*) \|', search_line)
    if result is not None:
        time_to_strip = result.group(1)
        if " " in time_to_strip:
            print("Invalid time format: {}".format(time_to_strip))
            continue
        target_seconds = sum(int(x) * 60 ** i for i,x in enumerate(reversed(time_to_strip.split(":"))))
        studysession_arr.append(target_seconds)

## Create total time studied array from "date time - date time"
studysession_date_arr = []
studysession_start_end_arr = []
pattern_datetime = '%d%m%Y%H%M%S'

for i in range(0,len(studytimes_arr)):
    search_line = studytimes_arr[i]
    result=re.search('([0-9][0-9]).([0-9][0-9]).([0-9][0-9][0-9][0-9]) ([0-9][0-9]):([0-9][0-9]):([0-9][0-9]) - ([0-9][0-9]).([0-9][0-9]).([0-9][0-9][0-9][0-9]) ([0-9][0-9]):([0-9][0-9]):([0-9][0-9]) \|', search_line)
    if result is not None:
        start_datetime = "".join(result.groups()[0:6])
        end_datetime = "".join(result.groups()[6:12])
        if not(len(start_datetime) == 14 == len(end_datetime)):
            print("Invalid start time format: {}".format(start_datetime))
            print("Invalid end time format: {}".format(end_datetime))
            continue
        start_epoch = int(time.mktime(time.strptime(start_datetime, pattern_datetime)))
        end_epoch = int(time.mktime(time.strptime(end_datetime, pattern_datetime)))
        studysession_start_end_arr.append((start_epoch, end_epoch))
        target_seconds = end_epoch - start_epoch
        studysession_date_arr.append(target_seconds)


# Parse the 'Total: ' format
seconds_nparr = np.asarray(studysession_arr)
total_seconds = np.sum(seconds_nparr)

#total_conv_minutes, total_conv_seconds = divmod(int(total_seconds), 60)
#total_conv_hours, total_conv_minutes = divmod(total_conv_minutes, 60)
#totalStudied = "%d:%02d:%02d" % (total_conv_hours, total_conv_minutes, total_conv_seconds)
totalStudied = epoch_to_HHMMSS(total_seconds)

# Parse the 'date time - date time'
seconds_nparr_date = np.asarray(studysession_date_arr)
total_seconds_datetime = np.sum(seconds_nparr_date)

#total_conv_minutes, total_conv_seconds = divmod(int(total_seconds_datetime), 60)
#total_conv_hours, total_conv_minutes = divmod(total_conv_minutes, 60)
#totalStudied_datetime = "%d:%02d:%02d" % (total_conv_hours, total_conv_minutes, total_conv_seconds)
totalStudied_datetime = epoch_to_HHMMSS(total_seconds_datetime)

# See which lines differ from the bookkeeping
raw_lines_np = np.asarray(studytimes_arr[8:])
differ_array = np.equal(seconds_nparr_date, seconds_nparr)
differs_idx = np.where(False == differ_array)
print_diff = raw_lines_np[differs_idx]


## Build the suggestions here
# These are the array items that differ
study_date_nparr = np.asarray(studysession_start_end_arr)[differs_idx]
study_nparr = np.asarray(studysession_arr)[differs_idx]


# First we do "date time - date time" suggestions, since they're easier
pattern_hreadable = '%d.%m.%Y %H:%M:%S'
suggestions_dates = [epoch_to_HHMMSS(end-start) for start, end in study_date_nparr]

# TODO: Total from start / end not implemented yet,
#   since the previous one is the most useful
suggestions_ready = []
regex = re.compile(r"Total: (.*) \|'")
for orig_diff, sugg in zip(print_diff, suggestions_dates):
    suggestions_ready.append(re.sub(r'Total: (.*) \|', 'Total: ' + sugg + ' |', orig_diff))

print("\nResult from the 'Total: X:XX:XX'")
print("The total sum of sessions (epoch) is: {}".format(total_seconds))
print("The total sum of sessions (HH:MM:SS) is: {}".format(totalStudied)) 


print("\nResult from the 'DDMMYYYY HH:MM:SS - DDMMYYYY HH:MM:SS'")
print("The total sum of sessions (epoch) is: {}".format(total_seconds_datetime))
print("The total sum of sessions (HH:MM:SS) is: {}".format(totalStudied_datetime)) 

if print_diff.size > 0:
    print("\n")
    print("Found incoherencies!\n")
    print("These lines are indifferent when comparing both methods:")
    for item in print_diff:
        print(item)
    
    print("\nPrinting suggestions based on the DD.MM.YYYY HH:MM:SS format:")
    for item in suggestions_ready:
        print(item)
else:
    print("\nNo incoherencies found!")