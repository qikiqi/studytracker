import re
import sys
import time, datetime
import numpy as np
import pprint
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker


# This tool can be used to graph totals per subject
# TODO: Implement 'timeline' x = time(ddmmyy), y=amount(+subject) (BARGRAPH)
# TODO: Integrate this tool into the studytracker tool itself (ARGPARSE)
print("Current location is {}".format(sys.argv[0]))
studytimes_file = "/home/x230/sync/oma/pytho/studytracker/studytracker/studytimes.txt"
with open(studytimes_file, 'r') as f:
    studytimes_arr = f.read().splitlines()

studytimes_format = []
for i in range(8, len(studytimes_arr)):
    studytimes_format.append(
        [studytimes_arr[i].split('-', 1)[0]] + studytimes_arr[i].split('-', 1)[1].split('|'))

studytimes_format_epochs = []
for item in studytimes_format:
    search_line = item[2]
    search_subj = item[3]
    result = re.search('Total: (.*)', search_line)
    result_subj = re.search('Subject: (.*)', search_subj)
    if result is not None:
        time_to_strip = result.group(1).strip()
        subj_to_strip = result_subj.group(1).strip()
        if " " in time_to_strip:
            print("Invalid time format: {}".format(time_to_strip))
            continue
        target_seconds = sum(int(x) * 60 ** i for i,x in enumerate(reversed(time_to_strip.split(":"))))
        studytimes_format_epochs.append([subj_to_strip, target_seconds])

subj_duration_np = np.array(studytimes_format_epochs, dtype='object')
subjects = np.unique(subj_duration_np[:,0])
subj_codes = np.arange(0,len(subjects))
subj_stack = np.column_stack((subjects,subj_codes))
subj_dict = {a : b for a, b in subj_stack}
subj_dict_inv = {b : a for a, b in subj_stack}
subj_codes_vec = [subj_dict[x] for x in subj_duration_np[:,0]]
subj_codes_dur = np.column_stack((subj_codes_vec,subj_duration_np[:,1]))
final = subj_codes_dur.astype(int)


sums = np.bincount(final[:,0], weights=final[:,1])

# Sorted by the largest value
sort_idx = np.argsort(sums)
sorted_sums = [sums[x] for x in sort_idx]

sorting_sums = True
totS = sorted_sums if sorting_sums else sums

sums_hhmmss = []
for total_seconds in totS:
    print("tot_s", total_seconds)
    total_conv_minutes, total_conv_seconds = divmod(int(total_seconds), 60)
    total_conv_hours, total_conv_minutes = divmod(total_conv_minutes, 60)
    totalStudied = "%d:%02d:%02d" % (total_conv_hours, total_conv_minutes, total_conv_seconds)
    sums_hhmmss.append(totalStudied)

# TODO: Make this as the correspondings bars title
def timeformat(total_seconds,pos=None):
    total_conv_minutes, total_conv_seconds = divmod(int(total_seconds), 60)
    total_conv_hours, total_conv_minutes = divmod(total_conv_minutes, 60)
    return "%d:%02d:%02d" % (total_conv_hours, total_conv_minutes, total_conv_seconds)
    #return "{:02d}:{:02d}".format(h,m)

fig, ax = plt.subplots()
ax.yaxis.set_major_formatter(mticker.FuncFormatter(timeformat))

plt.bar([subj_dict_inv[x] for x in sort_idx], sorted_sums)
plt.xticks(rotation='vertical')
plt.ylabel('Y-Values')
plt.xlabel('Time [hh:mm]')

plt.show(block=True)
